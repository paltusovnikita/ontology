"""ontology URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from app1.views import *


urlpatterns = [
    path('', IndexPage.as_view()),
    path('creationenviroment/', OntologyCreationEnvironment.as_view()),
    path('posthandler/', PostHandler.as_view()), #Обработчик post запросов на изменение триплетов от creationenviroment
    path('getsrtuctureontology/', GetStructureOntology.as_view()) #Получение структуры классов и подклассов в json creationenviroment
]
