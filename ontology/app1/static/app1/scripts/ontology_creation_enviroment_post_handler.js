
function appendSpan()
{
  for (let li of tree.querySelectorAll('li')) {
    let span = document.createElement('span');
    li.prepend(span);
    span.append(span.nextSibling); // поместить текстовый узел внутрь элемента <span>
  }

  //  ловим клики на всём дереве
  tree.onclick = function(event) {

    if (event.target.tagName != 'SPAN') {
      return;
    }

    let childrenContainer = event.target.parentNode.querySelector('ul');
    if (!childrenContainer) return; // нет детей

    childrenContainer.hidden = !childrenContainer.hidden;
  }
}
function removeIndexes(arr, indexes){ //Удаление элементов по списку индексов из массива
  var newarray = [];
  for (var i=0; i<arr.length; i++)
  {
    var flag = true; //не входит
    for(var j =0; j<indexes.length; j++)
    {
      if(indexes[j]==i)
      {
        flag = false;
        break;
      }
    }
    if(flag==true)
    {
      newarray.push(arr[i])
    }
  }
  return newarray;
  
}
function loadStructureClass() //Получение структуры классов
{
    let xhr = new XMLHttpRequest();

    xhr.open('GET', '/ontology/getsrtuctureontology/?action=Class', false);
    
    try {
      xhr.send();
      if (xhr.status != 200) {
        alert(`Ошибка ${xhr.status}: ${xhr.statusText}`);
      }
    } catch(err) { // для отлова ошибок используем конструкцию try...catch вместо onerror
      alert("Запрос не удался");
    }
    var jsonStructClass = JSON.parse(xhr.response); //получение сруктуры классов из django
    
    //Теперь получаем стуртуру в виде [[name,parent], ...]
    var listOfStructure = [];
    console.log(jsonStructClass)
    for (var i=0; i<jsonStructClass.length; i++)
    {
      listOfStructure.push([jsonStructClass[i].name, jsonStructClass[i].parent] ) //Отделение префикса
    }
    //Получаем корневой ul
    var root_ul = document.getElementById("tree");
    root_ul.innerHTML="";
    var pos = [] //позиции, которые надо удалить
    var iter = 0;
    for (var i=0; i<listOfStructure.length; i++)
    {
     if(listOfStructure[i][1]=="#")
     {
      var li = document.createElement("li");
      var ul = document.createElement("ul");
      li.append(listOfStructure[i][0]);
      li.setAttribute('name', listOfStructure[i][0]);
      li.setAttribute('id', listOfStructure[i][0]);
      li.append(ul);
      root_ul.append(li);
      pos.push(iter);
     }
     iter++; 
    }
    //Удаление корневых элементов из списка
    listOfStructure = removeIndexes(listOfStructure, pos); //Удаление элементов, которые входили в root
    while(listOfStructure.length>0)
    {
      iter = 0;
      pos = [];
      list_li = root_ul.getElementsByTagName("li") //получаем все li["name"], чтобы отследить, куда прикреплять, следующий элемент
      for (var i=0; i<listOfStructure.length; i++)
      {
        for (var j=0; j<list_li.length; j++)
        {
          if(listOfStructure[i][1] == list_li[j].getAttribute("name"))
          {
            li = document.createElement("li");
            li.append(listOfStructure[i][0]);
            li.setAttribute('name', listOfStructure[i][0]);
            li.setAttribute('id', listOfStructure[i][0]);
            var ul = document.createElement("ul");
            li.append(ul);
            list_li[j].getElementsByTagName("ul")[0].append(li);
            pos.push(iter);
          }
        }
        iter++;
      }
      listOfStructure = removeIndexes(listOfStructure, pos);
    }
    appendSpan();
    
    var c = document.getElementById("tree").getElementsByTagName("span");
}

function loadStructureProperty() //Получение структуры совйств
{
    let xhr = new XMLHttpRequest();

    xhr.open('GET', '/ontology/getsrtuctureontology/?action=Property', false);
    
    try {
      xhr.send();
      if (xhr.status != 200) {
        alert(`Ошибка ${xhr.status}: ${xhr.statusText}`);
      }
    } catch(err) { // для отлова ошибок используем конструкцию try...catch вместо onerror
      alert("Запрос не удался");
    }
    var jsonStructClass = JSON.parse(xhr.response); //получение сруктуры классов из django
    
    //Теперь получаем стуртуру в виде [[name,parent], ...]
    var listOfStructure = [];
    console.log(jsonStructClass)
    for (var i=0; i<jsonStructClass.length; i++)
    {
      listOfStructure.push([jsonStructClass[i].name, jsonStructClass[i].parent] ) //Отделение префикса
    }
    //Получаем корневой ul
    var root_ul = document.getElementById("tree");
    root_ul.innerHTML="";
    var pos = [] //позиции, которые надо удалить
    var iter = 0;
    for (var i=0; i<listOfStructure.length; i++)
    {
     if(listOfStructure[i][1]=="#")
     {
      var li = document.createElement("li");
      var ul = document.createElement("ul");
      li.append(listOfStructure[i][0]);
      li.setAttribute('name', listOfStructure[i][0]);
      li.setAttribute('id', listOfStructure[i][0]);
      li.append(ul);
      root_ul.append(li);
      pos.push(iter);
     }
     iter++; 
    }
    //Удаление корневых элементов из списка
    listOfStructure = removeIndexes(listOfStructure, pos); //Удаление элементов, которые входили в root
    while(listOfStructure.length>0)
    {
      iter = 0;
      pos = [];
      list_li = root_ul.getElementsByTagName("li") //получаем все li["name"], чтобы отследить, куда прикреплять, следующий элемент
      for (var i=0; i<listOfStructure.length; i++)
      {
        for (var j=0; j<list_li.length; j++)
        {
          if(listOfStructure[i][1] == list_li[j].getAttribute("name"))
          {
            li = document.createElement("li");
            li.append(listOfStructure[i][0]);
            li.setAttribute('name', listOfStructure[i][0]);
            li.setAttribute('id', listOfStructure[i][0]);
            var ul = document.createElement("ul");
            li.append(ul);
            list_li[j].getElementsByTagName("ul")[0].append(li);
            pos.push(iter);
          }
        }
        iter++;
      }
      listOfStructure = removeIndexes(listOfStructure, pos);
    }
    appendSpan();
    
    var c = document.getElementById("tree").getElementsByTagName("span");
}


function addClass(elem) //Добавление класса
{
    var subclassName = document.getElementById("subClassName").value;
    var className = document.getElementById("className").options[document.getElementById("className").selectedIndex].text;
    var xhr = new XMLHttpRequest();
    var selectClass = document.getElementById("className")
    
    if (className == "#"){
      var body = 'action=' + "addClass" + '&ontology=' + 'firstontology' + '&classname=' + encodeURIComponent(className) + '&subclassname=' + encodeURIComponent(subclassName);
      xhr.open("POST", '/ontology/posthandler/', true);
      xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
      xhr.onreadystatechange = function() {
      }
      xhr.send(body);

      var option = document.createElement("option");
      option.setAttribute('id', subclassName);
      option.setAttribute('name',subclassName);
      option.textContent=subclassName;
      selectClass.append(option);
    }
    else
    {
      var body = 'action=' + "addSubClass" + '&ontology=' + 'firstontology' + '&classname=' + encodeURIComponent(className) + '&subclassname=' + encodeURIComponent(subclassName);
      xhr.open("POST", '/ontology/posthandler/', true);
      xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
      xhr.onreadystatechange = function() {
      }
      xhr.send(body);

      var option = document.createElement("option");
      option.setAttribute('id', subclassName);
      option.setAttribute('name', subclassName);
      option.textContent=subclassName;
      selectClass.append(option);
    }
    loadStructureClass();   
}

