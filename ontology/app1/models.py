from django.db import models

from django.dispatch import receiver
from django.contrib.auth.models import User
from django.conf import settings
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.db.models.signals import post_save

# Create your models here.
class CreatedOntology(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL,on_delete=models.CASCADE, unique=False, db_column='author')
    name = models.CharField(max_length=100, unique=True, blank=False, db_column='name') #Название онтологии
    description = models.TextField(max_length=1000, blank=True, db_column='description') #Описание онтологии

    data = models.DateTimeField(db_column='data') #Дата создания онтологии

    def __str__(self):
        return "ontology:{}".format(self.name)

class Triplets(models.Model): #Список триплетов в онтологии
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE,db_column='author') #Автор ограничения
    ontology = models.ForeignKey(CreatedOntology, on_delete=models.CASCADE,db_column='ontology') #Ссылка на онтологию, к которой относится ограничение

    object_source = models.CharField(max_length=100, blank=False, unique=False, db_column='object_source') # Первый объект
    object_property = models.CharField(max_length=100, blank=False, unique=False, db_column='object_property') # Свойство    
    object_destanation = models.CharField(max_length=100, blank=False, unique=False,db_column='object_destanation') # Второй объект
    
    data = models.DateTimeField(db_column='data') #Дата создания онтологии
    
    def __str__(self):
        return "ontology:{} triplet:{}-{}->{}".format(self.ontology, self.object_source, self.object_property, self.object_destanation)
    

class RoleRestrictions(models.Model): #Ораничение для концепт свойство концепт
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, db_column='author') #Автор ограничения
    ontology = models.ForeignKey(CreatedOntology, on_delete=models.CASCADE, db_column='ontology') #Ссылка на онтологию, к которой относится ограничение

    first_concept = models.CharField(max_length=100, blank=False, unique=False, db_column='first_concept') # Первый концепт в ограничение
    first_role_second = models.CharField(max_length=100, blank=False, unique=False, db_column='first_role_second') # Роль    
    second_concept = models.CharField(max_length=100, blank=False, unique=False, db_column='second_concept') # Второй концепт в ограничение
    
    data = models.DateTimeField(db_column='data') #Дата создания онтологии

    def __str__(self):
        return "ontology:{} Role: {}-{}->{}".format(self.ontology, self.first_concept, self.first_role_second, self.second_concept)
    

class SimpleRulesForOntology(models.Model): #Задание простого правила транзитивности
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE,db_column='author') #Автор записи
    ontology = models.ForeignKey(CreatedOntology, on_delete=models.CASCADE,db_column='ontology') #Ссылка на онтологию, к которой относится запись

    first_concept = models.CharField(max_length=100, blank=False, unique=False,db_column='first_concept') # Первый концепт в правиле
    first_role_second = models.CharField(max_length=100, blank=False, unique=False,db_column='first_role_second') # Роль    
    second_concept = models.CharField(max_length=100, blank=False, unique=False,db_column='second_concept') # Второй концепт в правиле
    second_role_third = models.CharField(max_length=100, blank=False, unique=False,db_column='second_role_third') # Роль
    third_concept = models.CharField(max_length=100, blank=False, unique=False,db_column='third_concept') # Третий концепт в правиле

    transitive_node = models.CharField(max_length=100, blank=False, unique=False,db_column='transitive_node') #соединяющий узел

    data = models.DateTimeField(db_column='data') # Дата создания правила

    def __str__(self):
        return "ontology:{} Rules: {}-{}->{}-{}->{}".format(self.ontology, self.first_concept, self.first_role_second, self.second_concept, self.second_role_third, self.third_concept)
    

##############################################################################################################################################
#################################                   МОДЕЛЬ ПОЛЬЗОВАТЕЛЯ                 ######################################################
##############################################################################################################################################


from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser
)




class MyUserManager(BaseUserManager):
    def create_user(self, email, date_of_birth, password=None):
        """
        Creates and saves a User with the given email, date of
        birth and password.
        """
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=self.normalize_email(email),
            date_of_birth=date_of_birth,
        )
        user.is_admin = True
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, date_of_birth, password=None):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """
        user = self.create_user(
            email,
            password=password,
            date_of_birth=date_of_birth,
            
        )
        
        user.is_admin = True
        user.save(using=self._db)
        return user


class MyUser(AbstractBaseUser):
    email = models.EmailField(
        verbose_name='email address',
        max_length=255,
        unique=True,
    )
    date_of_birth = models.DateField()
    avatar = models.ImageField(upload_to="user_photo",blank = True)
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)

    objects = MyUserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['date_of_birth']

    def __str__(self):
        return self.email

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        # Simplest possible answer: All admins are staff
        return self.is_admin






    
    


    
    



