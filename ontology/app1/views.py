from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import render, get_object_or_404
from django.views import View
from .forms import UploadFileForm
from django.conf import settings
import os
from app1.models import *
from owlready2 import *
import rdflib
from django.conf import settings
from datetime import datetime
import json



from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
# Create your views here.

def handle_uploaded_file(f):
    
    with open('ontology/app1/ontology_file/name.txt', 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)


#####################################################################################################################
###########################################     INDEX PAGE        ###################################################
#####################################################################################################################

class IndexPage(View):
    def get(self, request):
        form = UploadFileForm()
        return render(request, 'app1/index.html', {'form': form},)
    def post(self, request):
        print("post:",request.POST)
        """
        path_ontology_absolute_path = os.path.join(settings.PATH_TO_ONTOLOGE_FILES, request.POST[""]) #Создания пути для сохранения онтологии
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            handle_uploaded_file(request.FILES['file'])
            return HttpResponse('Good Load')
        else: 
            return HttpResponse('Bad load')
        """


#####################################################################################################################
###########################################     РЕДАКТОР ОНТОЛОГИЙ      #############################################
#####################################################################################################################

   

def addClass(mail, ontology, classname,subclassname): #Добавление класса
    objectAuthor = get_object_or_404(MyUser, email = mail) #получение объекта автора
    objectOntology = get_object_or_404(CreatedOntology, name = ontology) #получение объекта онтологии
    new_triplets = Triplets(author=objectAuthor, ontology=objectOntology, object_source= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#" + subclassname, object_property= "http://www.w3.org/1999/02/22-rdf-syntax-ns#type", object_destanation= "http://www.w3.org/2002/07/owl#Class", data = datetime.now())
    new_triplets.save()
    return True
    

def addSubClass(mail, ontology, classname, subclassname): #Добавление подкласса
    objectAuthor = get_object_or_404(MyUser, email = mail) #получение объекта автора
    objectOntology = get_object_or_404(CreatedOntology, name = ontology) #получение объекта онтологии
    new_triplets = Triplets(author=objectAuthor, ontology=objectOntology, object_source= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#" + subclassname, object_property= "http://www.w3.org/1999/02/22-rdf-syntax-ns#type", object_destanation= "http://www.w3.org/2002/07/owl#Class", data = datetime.now())
    
    new_triplets.save()
    new_triplets = Triplets(author=objectAuthor, ontology=objectOntology, object_source= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#" + subclassname, object_property= "http://www.w3.org/2000/01/rdf-schema#subClassOf", object_destanation= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#" + classname, data = datetime.now())
    new_triplets.save()
    return True
        

def addProperty(mail, ontology, propertyname,subpropertyname): #Добавление свойства
    objectAuthor = get_object_or_404(MyUser, email = mail) #получение объекта автора
    objectOntology = get_object_or_404(CreatedOntology, name = ontology) #получение объекта онтологии
    new_triplets = Triplets(author=objectAuthor, ontology=objectOntology, object_source= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#" + subpropertyname, object_property= "http://www.w3.org/1999/02/22-rdf-syntax-ns#type", object_destanation= "http://www.w3.org/2002/07/owl#ObjectProperty", data = datetime.now())
    new_triplets.save()
    return True
    

def addSubProperty(mail, ontology, propertyname, subpropertyname): #Добавление подсвойства
    objectAuthor = get_object_or_404(MyUser, email = mail) #получение объекта автора
    objectOntology = get_object_or_404(CreatedOntology, name = ontology) #получение объекта онтологии
    new_triplets = Triplets(author=objectAuthor, ontology=objectOntology, object_source= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#" + subpropertyname, object_property= "http://www.w3.org/1999/02/22-rdf-syntax-ns#type", object_destanation= "http://www.w3.org/2002/07/owl#ObjectProperty", data = datetime.now())
    
    new_triplets.save()
    new_triplets = Triplets(author=objectAuthor, ontology=objectOntology, object_source= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#" + subpropertyname, object_property= "http://www.w3.org/2000/01/rdf-schema#subPropertyOf", object_destanation= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#" + propertyname, data = datetime.now())
    new_triplets.save()
    return True


def getAllClasses(): #Получение всех элементов, имеющих тип класс
    allClasses = list(Triplets.objects.filter(object_property="http://www.w3.org/1999/02/22-rdf-syntax-ns#type", object_destanation="http://www.w3.org/2002/07/owl#Class").values_list('object_source', flat=True))
    if (len(allClasses)):
       allClasses = [i.split("#")[1] for i in allClasses]
    allClasses.insert(0,"#")
     #Добавление корневого элемента
    return allClasses

def getAllProperty(): #Получение всех элементов, имеющих тип свойство
    allProperty = list(Triplets.objects.filter(object_property="http://www.w3.org/1999/02/22-rdf-syntax-ns#type", object_destanation="http://www.w3.org/2002/07/owl#http://www.w3.org/2002/07/owl#Object#Property").values_list('object_source', flat=True))
    if (len(allProperty)):
       allProperty = [i.split("#")[1] for i in allProperty]
    allProperty.insert(0,"#")
     #Добавление корневого элемента
    return allProperty

class OntologyCreationEnvironment(View): #Отдается начальная страница
    def get(self, request):

        return render(request, 'app1/creation_enviroment_ontology.html', {"listOfClasses": getAllClasses()})

@method_decorator(csrf_exempt, name='dispatch')
class PostHandler(View):   
    def post(self, request): #Здесь обрабатываются события добавления, удаления класса, добавления, удаления событися ...
        print(request.POST)
        if request.user.is_authenticated:
            mail = request.user.email

            ##########################          Работа с классами              ##########################################

            if(request.POST.get("action")=="addClass" and request.POST.get("classname") == "#"): # Если добавление класса
                if(addClass(mail = mail, ontology= request.POST.get("ontology"), classname = request.POST.get("classname"),subclassname=request.POST.get("subclassname"))):
                    HttpResponse(status=200)
                else:
                    HttpResponse(status=404)

            if(request.POST.get("action")=="addSubClass" and request.POST.get("classname") != "#"): #Если добавление подкласса
                if(addSubClass(mail = mail, ontology= request.POST.get("ontology"),classname = request.POST.get("classname"), subclassname=request.POST.get("subclassname"))):
                    HttpResponse(status=200)
                else:
                    HttpResponse(status=404)
            #########################          Работа со свойствами            ##########################################
            
            if(request.POST.get("action")=="addProperty" and request.POST.get("proprtyname") == "#"): # Если добавление класса
                if(addProperty(mail = mail, ontology= request.POST.get("ontology"), propertyname = request.POST.get("propertyname"),subpropertyname=request.POST.get("subpropertyname"))):
                    HttpResponse(status=200)
                else:
                    HttpResponse(status=404)

            if(request.POST.get("action")=="addSubProperty" and request.POST.get("protpertyname") != "#"): #Если добавление подкласса
                if(addSubProperty(mail = mail, ontology= request.POST.get("ontology"),classname = request.POST.get("propertyname"), subpropertyname=request.POST.get("subproperyname"))):
                    HttpResponse(status=200)
                else:
                    HttpResponse(status=404)
            
            #########################          Работа с экземплярами            ##########################################

        return HttpResponse("hello")

def getStructure(action):
    if(action == "Class"):
        allClasses = getAllClasses() #Получение списка всех элементов, имеющих тип класс
        allSubClasses = list(Triplets.objects.filter(object_property="http://www.w3.org/2000/01/rdf-schema#subClassOf").values_list('object_source', flat=True))
        allSubClasses = [i.split('#')[1] for i in allSubClasses]
        principals = list(set(allClasses).symmetric_difference(allSubClasses)) #Список принципов
        principals.remove('#') #Удаление корневого элемента
        print("principals",principals)

        treeClassList = [] #Возвращаемое дерево классов в виде list
        
        for i in principals: #Добавление принциполов в список словарей
            treeClassList.append({'name': i, 'parent':'#'})
        
        allSubClassesAndParent = list(Triplets.objects.filter(object_property="http://www.w3.org/2000/01/rdf-schema#subClassOf").values_list('object_source','object_destanation')) #Список кортежей (класс, родительский_класс)
        print(allSubClassesAndParent)

        for i in allSubClassesAndParent: #Добавление классов и родителей
            treeClassList.append({'name': i[0].split("#")[1], 'parent':i[1].split("#")[1]})
        return treeClassList


class GetStructureOntology(View):
    def get(self, request):
        if(request.GET.get("action")=="Class"):
            classTreeList = getStructure("Class") #Получение структуры классов в виде[{name:"", parent:""}]
            return JsonResponse(classTreeList, safe=False)


    
