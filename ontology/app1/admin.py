from django.contrib import admin
from app1.models import *
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin
from app1.forms import *


# Register your models here.
admin.site.register(MyUser, UserAdmin)
admin.site.register([CreatedOntology,Triplets,RoleRestrictions,SimpleRulesForOntology])
